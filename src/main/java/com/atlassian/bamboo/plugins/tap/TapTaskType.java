package com.atlassian.bamboo.plugins.tap;

import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

public class TapTaskType implements TaskType {
    private final TestCollationService testCollationService;

    public TapTaskType(TestCollationService testCollationService) {
        this.testCollationService = testCollationService;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException {
        final CurrentBuildResult currentResult = taskContext.getBuildContext().getBuildResult();

        ErrorMemorisingInterceptor errorLines = new ErrorMemorisingInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);

        final String testFilePattern = StringUtils.defaultIfEmpty(taskContext.getConfigurationMap().get(TapTaskConfigurator.PATTERN), "**/*.tap");

        // Hack - https://jira.atlassian.com/browse/BAM-14084  (once fixed needs to be a version-conditional hack)
        final String testFilePath = taskContext.getRootDirectory().toURI().relativize(taskContext.getWorkingDirectory().toURI()).getPath() + testFilePattern;
        final boolean failFailedTodos = Boolean.valueOf(taskContext.getConfigurationMap().get(TapTaskConfigurator.FAIL_FAILED_TODOS));

        try {
            testCollationService.collateTestResults(taskContext, testFilePath, new TapReportCollector(failFailedTodos));

            return TaskResultBuilder.newBuilder(taskContext).checkTestFailures().build();
        } catch (Exception e) {
            throw new TaskException("Failed to execute task", e);
        } finally {
            currentResult.addBuildErrors(errorLines.getErrorStringList());
        }
    }
}
