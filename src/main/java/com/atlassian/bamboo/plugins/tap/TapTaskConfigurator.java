package com.atlassian.bamboo.plugins.tap;

import java.util.List;
import java.util.Map;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;

import com.google.common.collect.ImmutableList;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TapTaskConfigurator extends AbstractTaskConfigurator implements TaskTestResultsSupport {
    public static final String CTX_UI_CONFIG_BEAN = "uiConfigBean";
    public static final String PATTERN = "testPattern";
    public static final String FAIL_FAILED_TODOS = "failFailedTodos";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(PATTERN, FAIL_FAILED_TODOS);

    public UIConfigSupport uiConfigSupport;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAllOperations(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAllOperations(context);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    public void populateContextForAllOperations(@NotNull Map<String, Object> context) {
        context.put(CTX_UI_CONFIG_BEAN, uiConfigSupport);
    }

    @Override
    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition) {
        return true;
    }

    public void setUiConfigSupport(UIConfigSupport uiConfigSupport) {
        this.uiConfigSupport = uiConfigSupport;
    }
}
